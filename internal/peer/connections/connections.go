/*
Copyright IBM Corp. All Rights Reserved.

SPDX-License-Identifier: Apache-2.0
*/

package connections

import (
	"context"
	"crypto/tls"
	"fmt"
	"io/ioutil"
	"os"
	"time"

	pb "github.com/hyperledger/fabric-protos-go/peer"
	"github.com/hyperledger/fabric/common/flogging"
	"github.com/hyperledger/fabric/core/config"

	"github.com/hyperledger/fabric/internal/pkg/comm"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

// UndefinedParamValue defines what undefined parameters in the command line will initialise to
const UndefinedParamValue = ""

// const CmdRoot = "core"

var mainLogger = flogging.MustGetLogger("connections")
var logOutput = os.Stderr

var (
	defaultConnTimeout = 3 * time.Second
	// These function variables (xyzFnc) can be used to invoke corresponding xyz function
	// this will allow the invoking packages to mock these functions in their unit test cases

	// GetEndorserClientFnc is a function that returns a new endorser client connection
	// to the provided peer address using the TLS root cert file,
	// by default it is set to GetEndorserClient function
	GetEndorserClientFnc func(address, tlsRootCertFile string) (pb.EndorserClient, error)

	// GetPeerDeliverClientFnc is a function that returns a new deliver client connection
	// to the provided peer address using the TLS root cert file,
	// by default it is set to GetDeliverClient function
	GetPeerDeliverClientFnc func(address, tlsRootCertFile string) (pb.DeliverClient, error)

	// GetDeliverClientFnc is a function that returns a new deliver client connection
	// to the provided peer address using the TLS root cert file,
	// by default it is set to GetDeliverClient function
	// GetDeliverClientFnc func(address, tlsRootCertFile string) (pb.Deliver_DeliverClient, error)

	// GetDefaultSignerFnc is a function that returns a default Signer(Default/PERR)
	// by default it is set to GetDefaultSigner function
	// GetDefaultSignerFnc func() (msp.SigningIdentity, error)

	// GetBroadcastClientFnc returns an instance of the BroadcastClient interface
	// by default it is set to GetBroadcastClient function
	// GetBroadcastClientFnc func() (BroadcastClient, error)

	// GetCertificateFnc is a function that returns the client TLS certificate
	// GetCertificateFnc func() (tls.Certificate, error)
)

type CommonClient struct {
	*comm.GRPCClient
	Address string
	sn      string
}

func init() {
	GetEndorserClientFnc = GetEndorserClient
	// GetDefaultSignerFnc = GetDefaultSigner
	// GetBroadcastClientFnc = GetBroadcastClient
	// GetDeliverClientFnc = GetDeliverClient
	GetPeerDeliverClientFnc = GetPeerDeliverClient
	// GetCertificateFnc = GetCertificate
}

// // InitConfig initializes viper config
// func InitConfig(cmdRoot string) error {

// 	err := config.InitViper(nil, cmdRoot)
// 	if err != nil {
// 		return err
// 	}

// 	err = viper.ReadInConfig() // Find and read the config file
// 	if err != nil {            // Handle errors reading the config file
// 		// The version of Viper we use claims the config type isn't supported when in fact the file hasn't been found
// 		// Display a more helpful message to avoid confusing the user.
// 		if strings.Contains(fmt.Sprint(err), "Unsupported Config Type") {
// 			return errors.New(fmt.Sprintf("Could not find config file. "+
// 				"Please make sure that FABRIC_CFG_PATH is set to a path "+
// 				"which contains %s.yaml", cmdRoot))
// 		} else {
// 			return errors.WithMessagef(err, "error when reading %s config file", cmdRoot)
// 		}
// 	}

// 	return nil
// }

// // InitCrypto initializes crypto for this peer
// func InitCrypto(mspMgrConfigDir, localMSPID, localMSPType string) error {
// 	// Check whether msp folder exists
// 	fi, err := os.Stat(mspMgrConfigDir)
// 	if err != nil {
// 		return errors.Errorf("cannot init crypto, specified path \"%s\" does not exist or cannot be accessed: %v", mspMgrConfigDir, err)
// 	} else if !fi.IsDir() {
// 		return errors.Errorf("cannot init crypto, specified path \"%s\" is not a directory", mspMgrConfigDir)
// 	}
// 	// Check whether localMSPID exists
// 	if localMSPID == "" {
// 		return errors.New("the local MSP must have an ID")
// 	}

// 	// Init the BCCSP
// 	SetBCCSPKeystorePath()
// 	bccspConfig := factory.GetDefaultOpts()
// 	if config := viper.Get("peer.BCCSP"); config != nil {
// 		err = mapstructure.WeakDecode(config, bccspConfig)
// 		if err != nil {
// 			return errors.WithMessage(err, "could not decode peer BCCSP configuration")
// 		}
// 	}

// 	err = mspmgmt.LoadLocalMspWithType(mspMgrConfigDir, bccspConfig, localMSPID, localMSPType)
// 	if err != nil {
// 		return errors.WithMessagef(err, "error when setting up MSP of type %s from directory %s", localMSPType, mspMgrConfigDir)
// 	}

// 	return nil
// }

// // SetBCCSPKeystorePath sets the file keystore path for the SW BCCSP provider
// // to an absolute path relative to the config file
// func SetBCCSPKeystorePath() {
// 	viper.Set("peer.BCCSP.SW.FileKeyStore.KeyStore",
// 		config.GetPath("peer.BCCSP.SW.FileKeyStore.KeyStore"))
// }

// // GetDefaultSigner return a default Signer(Default/PEER) for cli
// func GetDefaultSigner() (msp.SigningIdentity, error) {
// 	signer, err := mspmgmt.GetLocalMSP(factory.GetDefault()).GetDefaultSigningIdentity()
// 	if err != nil {
// 		return nil, errors.WithMessage(err, "error obtaining the default signing identity")
// 	}

// 	return signer, err
// }

// // Signer defines the interface needed for signing messages
// type Signer interface {
// 	Sign(msg []byte) ([]byte, error)
// 	Serialize() ([]byte, error)
// }

// // CheckLogLevel checks that a given log level string is valid
// func CheckLogLevel(level string) error {
// 	if !flogging.IsValidLevel(level) {
// 		return errors.Errorf("invalid log level provided - %s", level)
// 	}
// 	return nil
// }

func configFromEnv(prefix string) (address, override string, clientConfig comm.ClientConfig, err error) {
	address = viper.GetString(prefix + ".address")
	override = viper.GetString(prefix + ".tls.serverhostoverride")
	clientConfig = comm.ClientConfig{}
	connTimeout := viper.GetDuration(prefix + ".client.connTimeout")
	if connTimeout == time.Duration(0) {
		connTimeout = defaultConnTimeout
	}
	clientConfig.Timeout = connTimeout
	secOpts := comm.SecureOptions{
		UseTLS:            viper.GetBool(prefix + ".tls.enabled"),
		RequireClientCert: viper.GetBool(prefix + ".tls.clientAuthRequired"),
		TimeShift:         viper.GetDuration(prefix + ".tls.handshakeTimeShift"),
	}
	if secOpts.UseTLS {
		caPEM, res := ioutil.ReadFile(config.GetPath(prefix + ".tls.rootcert.file"))
		if res != nil {
			err = errors.WithMessage(res,
				fmt.Sprintf("unable to load %s.tls.rootcert.file", prefix))
			return
		}
		secOpts.ServerRootCAs = [][]byte{caPEM}
	}
	if secOpts.RequireClientCert {
		keyPEM, res := ioutil.ReadFile(config.GetPath(prefix + ".tls.clientKey.file"))
		if res != nil {
			err = errors.WithMessage(res,
				fmt.Sprintf("unable to load %s.tls.clientKey.file", prefix))
			return
		}
		secOpts.Key = keyPEM
		certPEM, res := ioutil.ReadFile(config.GetPath(prefix + ".tls.clientCert.file"))
		if res != nil {
			err = errors.WithMessage(res,
				fmt.Sprintf("unable to load %s.tls.clientCert.file", prefix))
			return
		}
		secOpts.Certificate = certPEM
	}
	clientConfig.SecOpts = secOpts
	return
}

// func InitCmd(cmd *cobra.Command, args []string) {
// 	err := InitConfig(CmdRoot)
// 	if err != nil { // Handle errors reading the config file
// 		mainLogger.Errorf("Fatal error when initializing %s config : %s", CmdRoot, err)
// 		os.Exit(1)
// 	}

// 	// read in the legacy logging level settings and, if set,
// 	// notify users of the FABRIC_LOGGING_SPEC env variable
// 	var loggingLevel string
// 	if viper.GetString("logging_level") != "" {
// 		loggingLevel = viper.GetString("logging_level")
// 	} else {
// 		loggingLevel = viper.GetString("logging.level")
// 	}
// 	if loggingLevel != "" {
// 		mainLogger.Warning("CORE_LOGGING_LEVEL is no longer supported, please use the FABRIC_LOGGING_SPEC environment variable")
// 	}

// 	loggingSpec := os.Getenv("FABRIC_LOGGING_SPEC")
// 	loggingFormat := os.Getenv("FABRIC_LOGGING_FORMAT")

// 	flogging.Init(flogging.Config{
// 		Format:  loggingFormat,
// 		Writer:  logOutput,
// 		LogSpec: loggingSpec,
// 	})

// 	// chaincode packaging does not require material from the local MSP
// 	if cmd.CommandPath() == "peer lifecycle chaincode package" {
// 		mainLogger.Debug("peer lifecycle chaincode package does not need to init crypto")
// 		return
// 	}

// 	// Init the MSP
// 	var mspMgrConfigDir = config.GetPath("peer.mspConfigPath")
// 	var mspID = viper.GetString("peer.localMspId")
// 	var mspType = viper.GetString("peer.localMspType")
// 	if mspType == "" {
// 		mspType = msp.ProviderTypeToString(msp.FABRIC)
// 	}
// 	err = InitCrypto(mspMgrConfigDir, mspID, mspType)
// 	if err != nil { // Handle errors reading the config file
// 		mainLogger.Errorf("Cannot run peer because %s", err.Error())
// 		os.Exit(1)
// 	}
// }

// PeerClient represents a client for communicating with a peer
type PeerClient struct {
	CommonClient
}

// NewPeerClientFromEnv creates an instance of a PeerClient from the global
// Viper instance
func NewPeerClientFromEnv() (*PeerClient, error) {
	address, override, clientConfig, err := configFromEnv("peer")
	if err != nil {
		return nil, errors.WithMessage(err, "failed to load config for PeerClient")
	}
	return newPeerClientForClientConfig(address, override, clientConfig)
}

// NewPeerClientForAddress creates an instance of a PeerClient using the
// provided peer address and, if TLS is enabled, the TLS root cert file
func NewPeerClientForAddress(address, tlsRootCertFile string) (*PeerClient, error) {
	if address == "" {
		return nil, errors.New("peer address must be set")
	}

	override := viper.GetString("peer.tls.serverhostoverride")
	clientConfig := comm.ClientConfig{}
	clientConfig.Timeout = viper.GetDuration("peer.client.connTimeout")
	if clientConfig.Timeout == time.Duration(0) {
		clientConfig.Timeout = defaultConnTimeout
	}

	secOpts := comm.SecureOptions{
		UseTLS:            viper.GetBool("peer.tls.enabled"),
		RequireClientCert: viper.GetBool("peer.tls.clientAuthRequired"),
	}

	if secOpts.RequireClientCert {
		keyPEM, err := ioutil.ReadFile(config.GetPath("peer.tls.clientKey.file"))
		if err != nil {
			return nil, errors.WithMessage(err, "unable to load peer.tls.clientKey.file")
		}
		secOpts.Key = keyPEM
		certPEM, err := ioutil.ReadFile(config.GetPath("peer.tls.clientCert.file"))
		if err != nil {
			return nil, errors.WithMessage(err, "unable to load peer.tls.clientCert.file")
		}
		secOpts.Certificate = certPEM
	}
	clientConfig.SecOpts = secOpts

	if clientConfig.SecOpts.UseTLS {
		if tlsRootCertFile == "" {
			return nil, errors.New("tls root cert file must be set")
		}
		caPEM, res := ioutil.ReadFile(tlsRootCertFile)
		if res != nil {
			return nil, errors.WithMessagef(res, "unable to load TLS root cert file from %s", tlsRootCertFile)
		}
		clientConfig.SecOpts.ServerRootCAs = [][]byte{caPEM}
	}
	return newPeerClientForClientConfig(address, override, clientConfig)
}

func newPeerClientForClientConfig(address, override string, clientConfig comm.ClientConfig) (*PeerClient, error) {
	// set the default keepalive options to match the server
	clientConfig.KaOpts = comm.DefaultKeepaliveOptions
	gClient, err := comm.NewGRPCClient(clientConfig)
	if err != nil {
		return nil, errors.WithMessage(err, "failed to create PeerClient from config")
	}
	pClient := &PeerClient{
		CommonClient: CommonClient{
			GRPCClient: gClient,
			Address:    address,
			sn:         override}}
	return pClient, nil
}

// Endorser returns a client for the Endorser service
func (pc *PeerClient) Endorser() (pb.EndorserClient, error) {
	conn, err := pc.CommonClient.NewConnection(pc.Address, comm.ServerNameOverride(pc.sn))
	if err != nil {
		return nil, errors.WithMessagef(err, "endorser client failed to connect to %s", pc.Address)
	}
	return pb.NewEndorserClient(conn), nil
}

// Deliver returns a client for the Deliver service
func (pc *PeerClient) Deliver() (pb.Deliver_DeliverClient, error) {
	conn, err := pc.CommonClient.NewConnection(pc.Address, comm.ServerNameOverride(pc.sn))
	if err != nil {
		return nil, errors.WithMessagef(err, "deliver client failed to connect to %s", pc.Address)
	}
	return pb.NewDeliverClient(conn).Deliver(context.TODO())
}

// PeerDeliver returns a client for the Deliver service for peer-specific use
// cases (i.e. DeliverFiltered)
func (pc *PeerClient) PeerDeliver() (pb.DeliverClient, error) {
	conn, err := pc.CommonClient.NewConnection(pc.Address, comm.ServerNameOverride(pc.sn))
	if err != nil {
		return nil, errors.WithMessagef(err, "deliver client failed to connect to %s", pc.Address)
	}
	return pb.NewDeliverClient(conn), nil
}

// Certificate returns the TLS client certificate (if available)
func (pc *PeerClient) Certificate() tls.Certificate {
	return pc.CommonClient.Certificate()
}

// GetEndorserClient returns a new endorser client. If the both the address and
// tlsRootCertFile are not provided, the target values for the client are taken
// from the configuration settings for "peer.address" and
// "peer.tls.rootcert.file"
func GetEndorserClient(address, tlsRootCertFile string) (pb.EndorserClient, error) {
	var peerClient *PeerClient
	var err error
	if address != "" {
		peerClient, err = NewPeerClientForAddress(address, tlsRootCertFile)
	} else {
		peerClient, err = NewPeerClientFromEnv()
	}
	if err != nil {
		return nil, err
	}
	return peerClient.Endorser()
}

// GetCertificate returns the client's TLS certificate
func GetCertificate() (tls.Certificate, error) {
	peerClient, err := NewPeerClientFromEnv()
	if err != nil {
		return tls.Certificate{}, err
	}
	return peerClient.Certificate(), nil
}

// GetDeliverClient returns a new deliver client. If both the address and
// tlsRootCertFile are not provided, the target values for the client are taken
// from the configuration settings for "peer.address" and
// "peer.tls.rootcert.file"
func GetDeliverClient(address, tlsRootCertFile string) (pb.Deliver_DeliverClient, error) {
	var peerClient *PeerClient
	var err error
	if address != "" {
		peerClient, err = NewPeerClientForAddress(address, tlsRootCertFile)
	} else {
		peerClient, err = NewPeerClientFromEnv()
	}
	if err != nil {
		return nil, err
	}
	return peerClient.Deliver()
}

// GetPeerDeliverClient returns a new deliver client. If both the address and
// tlsRootCertFile are not provided, the target values for the client are taken
// from the configuration settings for "peer.address" and
// "peer.tls.rootcert.file"
func GetPeerDeliverClient(address, tlsRootCertFile string) (pb.DeliverClient, error) {
	var peerClient *PeerClient
	var err error
	if address != "" {
		peerClient, err = NewPeerClientForAddress(address, tlsRootCertFile)
	} else {
		peerClient, err = NewPeerClientFromEnv()
	}
	if err != nil {
		return nil, err
	}
	return peerClient.PeerDeliver()
}
