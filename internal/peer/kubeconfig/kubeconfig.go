// mapping of various kubernetes configurations and variables.

package kubeconfig

import (
	"log"
	"strconv"

	"github.com/hyperledger/fabric-protos-go/peer"
	"github.com/hyperledger/fabric/internal/peer/connections"
	"github.com/spf13/viper"
)

// var logger := flogging.MustGetLogger("kubeconfig")
var channel_peer_map map[string]string
var peer_copy_used int
var channel_endoser_map map[string]peer.EndorserClient
var channel_deliver_filtered_map map[string]peer.DeliverClient

//Initialize c
func Initialize() {
	peer_copy_used = 0
	channel_peer_map = make(map[string]string)
	channel_endoser_map = make(map[string]peer.EndorserClient)
	channel_deliver_filtered_map = make(map[string]peer.DeliverClient)
}
func initConnections(peerAddr string, cName string) error {

	endorserClient, err := connections.GetEndorserClientFnc(peerAddr+":7051", viper.GetString("peer.tls.rootcert.file"))
	if err != nil {
		log.Println("get Endoser Client failed %s", peerAddr)
		return err
	}
	channel_endoser_map[cName] = endorserClient

	deliverClient, err := connections.GetPeerDeliverClientFnc(peerAddr+":7051", viper.GetString("peer.tls.rootcert.file"))
	if err != nil {
		log.Println("GetPeerDeliverClient failed %s", peerAddr)
		return err
	}
	channel_deliver_filtered_map[cName] = deliverClient

	return nil
}

func GetEndorserClient(cName string) peer.EndorserClient {
	return channel_endoser_map[cName]
}

func GetPeerDeliverClient(cName string) peer.DeliverClient {
	return channel_deliver_filtered_map[cName]
}
func LinkChannelPeer(cname string) bool {
	// if (contains(channel_list, cname )){
	// 	//new channel
	// }
	peerName := "peer0-org6-copy" + strconv.Itoa(peer_copy_used)
	log.Println("KUBECONFIG - ", peerName, "linked to channel", cname)
	peer_copy_used = peer_copy_used + 1
	channel_peer_map[cname] = peerName
	log.Println(peerName, "linked to channel", cname)
	initConnections(peerName, cname)
	return true
}

func GetCopyPeerAddress(channelName string) string {
	return channel_peer_map[channelName]
}

func GetAllCopyPeers() []string {
	v := make([]string, 0)

	for _, value := range channel_peer_map {
		v = append(v, value)
	}
	return v
}

func GetAllChannels() []string {
	v := make([]string, 0)

	for cname, _ := range channel_peer_map {
		v = append(v, cname)
	}
	return v
}

// func contains(arr []string, str string) bool {
// 	for _, a := range arr {
// 	   if a == str {
// 		  return true
// 	   }
// 	}
// 	return false
//  }

// //Increment c
// func Increment() {
// 	counter++
// }

// //Value c
// func Value() int {
// 	return counter
// }
