# Aramid - A privacy preserving enhancement to fabric

For setting up the environment and compailing code please refer the [original readme](README_ORG)

This projects gitlab branch structure
```plaintext
|-- aramid
|   |-- branch
|       |-- main
|       |-- release-2.2
```


From this code we can create the modified privacy enhanced peer-docker container. We are asuming you 
have installed all the necessary packages as per the [original readme](README_ORG). Create a docker id
so that you can push your modified images and run experiments on them on a different kubernative cluster environment.

#### for compiling and pushing aramid to dockerhub
    make peer-docker 

    #use your credentials for login
    docker login 

    docker tag 'hash_of_image' 'aramiddockerhub'/fabric-peer:2.2aramid
    docker tag 'aramiddockerhub'/fabric-peer 'aramiddockerhub'/fabric-peer:2.2custom
    docker push 'aramiddockerhub'/fabric-peer:2.2custom
